﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public static class Application
    {
        public static void Run()
        {
            var menu = new StringBuilder();
            menu.Append("Hello, welcome to my App:\n");

            menu.AppendLine("Enter your First Name");
            Console.WriteLine(menu.ToString());
            var firstName = Console.ReadLine().Trim();
            while (ValidateStudent(firstName))
            {
                Console.WriteLine("Please enter a valid First Name!");
                firstName = Console.ReadLine().Trim();
            }

            Console.WriteLine("Enter Lastname");
            var lastName = Console.ReadLine().Trim();
            while (ValidateStudent(lastName))
            {
                Console.WriteLine("Please enter a valid Last Name!");
                lastName = Console.ReadLine().Trim();
            }

            Console.WriteLine("Please enter your email address(name@domain.com)");
            var email = Console.ReadLine().Trim();
            while (ValidateEmail(email))
            {
                Console.WriteLine("please input a valid email address(name@domain.com)");
                email = Console.ReadLine().Trim();
            }

            Console.WriteLine("Please enter your birthday(DD/MM/YYYY)");
            var birthday = Console.ReadLine().Trim();
            while (!isValidDate(birthday))
            {
                Console.WriteLine("pls enter a valid date");
                birthday = Console.ReadLine().Trim();
            }

            Console.WriteLine("Select your Gender: \n 1. male \n 2. Female \n 3. Prefer not to say");
            var gender = Console.ReadLine().Trim();
            while (gender != "1" && gender != "2" && gender != "3")
            {
                Console.WriteLine("Invalid Gender Selection\nPlease enter a valid Gender.");
                gender = Console.ReadLine().Trim();
            }
            var selectedGender = GenderSelection(gender);


            Console.WriteLine("Enter Password");
            var password = Console.ReadLine();
            while (IsBlank(password))
            {
                Console.WriteLine($"Please input a valid password: ");
                password = Console.ReadLine();
            }


            Console.WriteLine("Confirm Password");
            var confirmPassword = Console.ReadLine();
            while (IsBlank(confirmPassword))
            {
                Console.WriteLine($"Please Re-Enter a valid Password");
                confirmPassword = Console.ReadLine();
                
            }
            while (password != confirmPassword)
            {
                Console.WriteLine($"Please enter the same password");
                confirmPassword = Console.ReadLine();
            }
            if (password == confirmPassword)
            {
                var formData = new Register
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    Birthday = DateTime.Parse(birthday),
                    Password = password,
                    ConfirmPassword = confirmPassword,
                    Gender = selectedGender
                };

                AccountService.Register(formData);
            }
        }

        public static Gender GenderSelection(string gender)
        {
            
            switch (gender)
            {
                case "1":
                    return Gender.Male;       
                case "2":
                    return Gender.Female;
                case "3":
                    return Gender.PreferNotToSay;
                default:
                    return Gender.SelectGender;
            }
        }
        static bool ValidateEmail(string text)
        {
            // define regex with email pattern
            Regex regex = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);

            if (regex.IsMatch(text))
            return false;
            return true;
        }
        private static bool ValidateStudent(string std)
        {
            Regex regex = new Regex("^[a-zA-Z]+$");
            if (regex.IsMatch(std))
                return false;
            return true;
        }
        private static bool isValidDate(string date)
        {
            DateTime result;
            if (!DateTime.TryParse(date, out result) || string.IsNullOrWhiteSpace(date))
                return false;
            return true;
        }
        private static bool IsBlank(string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
                return false;
            return true;
        }



    }
}
